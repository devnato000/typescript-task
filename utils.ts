import {IStudent} from "./interfaces.js";

export function calculateTotalCredits(student: IStudent): number {
	let totalCredits = 0;

	for (const course of student.courses) {
		totalCredits += course.credits;
	}

	return totalCredits;
}

export function uid() {
	return Math.floor(Math.random() * 1000)
}

export function logMethod(target: any, key: string, descriptor: PropertyDescriptor): void {
	const originalMethod = descriptor.value;

	descriptor.value = function (...args: any[]) {
		console.log(`Calling method ${key} with arguments:`, args);
		return originalMethod.apply(this, args);
	};
}

export function isStudent(obj: any): obj is IStudent {
	return obj && obj.name && typeof obj.name === "string" && obj.courses && typeof obj._grant === "boolean";
}
