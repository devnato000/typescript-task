import {Semester} from './enums.js'
import {
	IStudent,
	IProfessor,
	ICourse,
	Transcript,
} from './interfaces'
import {
	UniversityDepartments,
	UniversityJobs,
	StudentImpl,
	MasterStudent,
	ProfessorImpl
} from './classes.js'
import {calculateTotalCredits, isStudent} from './utils.js'

const physicsCourse: ICourse = {
	courseId: 1,
	courseName: "Physics 101",
	department: "Physics",
	semester: Semester.Spring,
	credits: 1,
	score: 0
};
const mathCourse: ICourse = {
	courseId: 2,
	courseName: "Mathematics 201",
	department: "Mathematics",
	semester: Semester.Fall,
	credits: 2,
	score: 0
};
const computerScience: ICourse = {
	courseId: 3,
	courseName: "Computer Science 301",
	department: "Computer Science",
	semester: Semester.Fall,
	credits: 3,
	score: 0
};
const softwareArchitecture: ICourse = {
	courseId: 4,
	courseName: "Software Architecture 401",
	department: "Software Architecture",
	semester: Semester.Fall,
	credits: 4,
	score: 0
};

const myUniversity: UniversityDepartments = new UniversityDepartments("mY University", "City", [
	"Software Architecture",
	"Computer Science",
	"Mathematics",
	"Physics",
]);
const myUniversityJobs: UniversityJobs = new UniversityJobs()
console.log(myUniversity.getUniversityInfo());

myUniversityJobs.addJob('teacher', 1000)
console.log(myUniversityJobs.showJobs())

const smith: IStudent = new StudentImpl("Smith", 19, "Male");
const john: IStudent = new StudentImpl("John", 20, "Male");

console.log(john)

john.enroll(physicsCourse);
john.enroll(mathCourse);
john.rateCourse(mathCourse, 60);
john.rateCourse(physicsCourse, 100);
john.drop(physicsCourse);
const johnTranscript: Transcript = john.getTranscript();
const totalJohnCredits: number = calculateTotalCredits(john);
console.log(`Total credits for student John: ${totalJohnCredits}`);
console.log('GPA(Grade Point Average) : ', johnTranscript.calculateGPA());
console.log('isStudent : ', isStudent(smith))
smith.enroll(computerScience);
smith.grant = true
smith.grant

const edward = new MasterStudent("Edward", 21, "Male", softwareArchitecture);
edward.chooseMainCourse(softwareArchitecture)

const jane: IProfessor = new ProfessorImpl("Jane", 35, "Female", 10, 20);
const jack: IProfessor = new ProfessorImpl("Jack", 45, "Male", 20, 19);

jane.teach(physicsCourse);

jack.teach(mathCourse);
jack.teach(computerScience);
jack.teach(softwareArchitecture);


jack.getSalary()
jane.getSalary()
