import {
	IStudent,
	IProfessor,
	ICourse,
	Transcript,
	TranscriptCourse
} from './interfaces.js'
import {Gender, Department, Grade, Job} from './types.js'
import {uid, logMethod} from "./utils.js";

abstract class University {
	readonly name: string;
	readonly location: string;

	protected constructor(name: string, location: string) {
		this.name = name;
		this.location = location;
	}
}

class UniversityDepartments extends University {
	readonly departments: Department[];

	constructor(name: string, location: string, departments: Department[]) {
		super(name, location);
		this.departments = departments;
	}

	public getUniversityInfo(): string {
		return `University Name : ${this.name}\nLocation : ${this.location}\nDepartments : ${this.departments} `;
	}
}

class UniversityJobs extends University {
	public jobsType: Job[]

	constructor() {
		super("universityJobs", "",);
		this.jobsType = [];
	}

	@logMethod
	public addJob(jobName: string, salary: number) {
		this.jobsType.push([jobName, salary])
	}

	public showJobs() {
		return `all available jobs :  ${this.jobsType}`
	}
}

class StudentImpl implements IStudent {
	public name: string;
	public age: number;
	public gender: Gender;
	readonly studentId: number;
	public courses: ICourse [];
	_grant: boolean

	constructor(name: string, age: number, gender: Gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.studentId = uid();
		this.courses = [];
		this._grant = false
	}

	get grant() {
		console.log(`${this.name} ${this._grant ? 'has grant' : 'hasnt'}`)
		return this._grant
	}

	set grant(value) {
		this._grant = value
	}

	public enroll(course: ICourse): void {
		this.courses.push(course);
		console.log(`Student ${this.name} enrolled in ${course.courseName}`);
	}

	public drop(course: ICourse): void {
		const index = this.courses.findIndex((c) => c.courseId === course.courseId);
		if (index !== -1) {
			this.courses.splice(index, 1);
			console.log(`Student ${this.name} dropped ${course.courseName}`);
		} else {
			console.log(`Student ${this.name} is not enrolled in ${course.courseName}`);
		}
	}

	public rateCourse(course: ICourse, score: number): void {
		const currCourse = this.courses.find(({courseId}) => courseId === course.courseId)
		// @ts-ignore
		currCourse.score = score
		console.log(`Student ${this.name} now have ${course.score} for ${course.courseName}`);
	}

	public getTranscript(): Transcript {
		const transcriptCourses: TranscriptCourse[] = this.courses.map((course) => {
			const grade: Grade = this.calculateGrade(course);
			return {course, grade};
		});

		return {
			student: this,
			courses: transcriptCourses,
			calculateGPA() {
				const totalCourses = this.courses.length;
				let totalPoints = 0;
				for (const transcriptCourse of this.courses) {
					const {course, grade} = transcriptCourse;
					totalPoints += StudentImpl.calculateGradePoints(grade);
				}
				return totalPoints / totalCourses
			},
		};
	}

	private calculateGrade(course: ICourse): Grade {
		const averageScore = this.getCourseScore(course);

		if (averageScore >= 90) {
			return "A";
		} else if (averageScore >= 80) {
			return "B";
		} else if (averageScore >= 70) {
			return "C";
		} else if (averageScore >= 60) {
			return "D";
		} else {
			return "F";
		}
	}

	private getCourseScore(course: ICourse): number {
		return course.score
	}

	private static calculateGradePoints(grade: Grade): number {
		switch (grade) {
			case "A":
				return 4;
			case "B":
				return 3;
			case "C":
				return 2;
			case "D":
				return 1;
			case "F":
				return 0;
			default:
				return 0;
		}
	}
}

class MasterStudent extends StudentImpl {
	public mainCourse: ICourse

	constructor(name: string, age: number, gender: Gender, mainCourse: ICourse) {
		super(name, age, gender);
		this.mainCourse = mainCourse
	}

	public chooseMainCourse(course: ICourse): void {
		this.mainCourse = course;
		this.showMainCourse()
	}

	public showMainCourse() {
		console.log(`this is ${this.name} main course : ${this.mainCourse?.courseName}`)
	}
}

class ProfessorImpl implements IProfessor {
	public name: string;
	public age: number;
	public gender: Gender;
	public experience: number;
	public workDays: number
	readonly professorId: number;
	public coursesTeaching: ICourse[];

	constructor(name: string, age: number, gender: Gender, experience: number, workDays: number) {
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.experience = experience
		this.workDays = workDays
		this.professorId = uid();
		this.coursesTeaching = [];
	}

	public teach(course: ICourse): void {
		this.coursesTeaching.push(course);
		console.log(`Professor ${this.name} is teaching ${course.courseName}`);
	}

	public getSalary(): void {
		console.log(`Professor ${this.name} salary is ${this.calcSalary()}$`)
	}

	private calcSalary() {
		const moneyPerCourse = 50
		const bonusPerExperience = this.experience / 100
		return (this.coursesTeaching.length + bonusPerExperience) * moneyPerCourse * this.workDays
	}
}

export {
	UniversityDepartments,
	UniversityJobs,
	StudentImpl,
	MasterStudent,
	ProfessorImpl
}