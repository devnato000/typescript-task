import {Semester} from "./enums.js";
import {Department, Gender, Grade} from "./types.js";

interface IPerson {
	name: string;
	age: number;
	gender: Gender;
}

interface IStudent extends IPerson {
	studentId: number;
	courses: ICourse[];

	get grant()

	set grant(value: boolean)

	enroll(course: ICourse): void;

	drop(course: ICourse): void;

	rateCourse(course: ICourse, score: number): void

	getTranscript(): Transcript;

}

interface IProfessor extends IPerson {
	professorId: number;
	coursesTeaching: ICourse[];
	experience: number;
	workDays: number

	teach(course: ICourse): void;

	getSalary(): void;
}

interface ICourse {
	readonly courseId: number;
	readonly courseName: string;
	readonly department: Department;
	semester: Semester;
	credits: number;
	score: number
}


interface Transcript {
	student: IStudent;
	courses: TranscriptCourse[];

	calculateGPA(): number;
}

interface TranscriptCourse {
	course: ICourse;
	grade: Grade;
}

export {IStudent, IProfessor, ICourse, Transcript, TranscriptCourse}