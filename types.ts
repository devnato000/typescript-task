type Gender = "Male" | "Female";

type Department = "Computer Science" | "Mathematics" | "Physics" |
		"Software Architecture";

type Grade = "A" | "B" | "C" | "D" | "F";

type Job = [string, number]

export {Gender, Department, Grade, Job}