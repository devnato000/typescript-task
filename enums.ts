enum Semester {
	Spring = "Spring",
	Summer = "Summer",
	Fall = "Fall",
	Winter = "Winter",
}

export {Semester}